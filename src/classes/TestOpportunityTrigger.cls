@isTest
private class TestOpportunityTrigger {
	
	@isTest 
	static void testUpdateFundAmountsAfterInsert() {
		// Test data setup
		Fund__c testFund = new Fund__c();
		testFund.Name = 'Fund 1';

		insert testFund;

		Opportunity opp1 = new Opportunity(
				Name='Opportunity test 1',
				Amount=10000,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp2 = new Opportunity(
				Name='Opportunity test 2',
				Amount=100,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp3 = new Opportunity(
			Name='Opportunity test 3',
			Amount=500,
			StageName='Closed Won',
			CloseDate=System.today().addMonths(1),
			Fund__c=testFund.Id
		);

		// Perform test
        Test.startTest();
        insert opp1;
		insert opp2;
		insert opp3;
        Test.stopTest();

		// Verify
		Fund__c fundAfterInsert = [Select Cleared_Investment_Amount__c, Pipeline_Investment_Amount__c from Fund__c where Id= :testFund.Id];
		System.assertEquals(500.00, fundAfterInsert.Cleared_Investment_Amount__c);
		System.assertEquals(10100.00, fundAfterInsert.Pipeline_Investment_Amount__c);

	}

	@isTest 
	static void testUpdateFundAmountsAfterUpdate() {
		// Test data setup
		Fund__c testFund = new Fund__c();
		testFund.Name = 'Fund 1';

		insert testFund;

		Opportunity opp1 = new Opportunity(
				Name='Opportunity test 1',
				Amount=10000,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp2 = new Opportunity(
				Name='Opportunity test 2',
				Amount=100,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp3 = new Opportunity(
			Name='Opportunity test 3',
			Amount=500,
			StageName='Closed Won',
			CloseDate=System.today().addMonths(1),
			Fund__c=testFund.Id
		);

		insert opp1;
		insert opp2;
		insert opp3;

		// Perform test
		Test.startTest();
		opp1.Amount = 30;
		update opp1;
		Test.stopTest();

		// Verify
		Fund__c fundAfterUpdate = [Select Cleared_Investment_Amount__c, Pipeline_Investment_Amount__c from Fund__c where Id= :testFund.Id];
		System.assertEquals(500.00, fundAfterUpdate.Cleared_Investment_Amount__c);
		System.assertEquals(130.00, fundAfterUpdate.Pipeline_Investment_Amount__c);

	}

	@isTest 
	static void testUpdateFundAmountsAfterDelete() {
		// Test data setup
		Fund__c testFund = new Fund__c();
		testFund.Name = 'Fund 1';

		insert testFund;

		Opportunity opp1 = new Opportunity(
				Name='Opportunity test 1',
				Amount=10000,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp2 = new Opportunity(
				Name='Opportunity test 2',
				Amount=100,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp3 = new Opportunity(
			Name='Opportunity test 3',
			Amount=500,
			StageName='Closed Won',
			CloseDate=System.today().addMonths(1),
			Fund__c=testFund.Id
		);

		insert opp1;
		insert opp2;
		insert opp3;

		// Perform test
		Test.startTest();
		delete opp1;
		Test.stopTest();

		// Verify
		Fund__c fundAfterUpdate = [Select Cleared_Investment_Amount__c, Pipeline_Investment_Amount__c from Fund__c where Id= :testFund.Id];
		System.assertEquals(500.00, fundAfterUpdate.Cleared_Investment_Amount__c);
		System.assertEquals(100.00, fundAfterUpdate.Pipeline_Investment_Amount__c);

	}

	@isTest 
	static void testUpdateFundAmountsAfterUndelete() {
		// Test data setup
		Fund__c testFund = new Fund__c();
		testFund.Name = 'Fund 1';

		insert testFund;

		Opportunity opp1 = new Opportunity(
				Name='Opportunity test 1',
				Amount=10000,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp2 = new Opportunity(
				Name='Opportunity test 2',
				Amount=100,
				StageName='Prospecting',
				CloseDate=System.today().addMonths(1),
				Fund__c=testFund.Id
			);

		Opportunity opp3 = new Opportunity(
			Name='Opportunity test 3',
			Amount=500,
			StageName='Closed Won',
			CloseDate=System.today().addMonths(1),
			Fund__c=testFund.Id
		);

		insert opp1;
		insert opp2;
		insert opp3;
		delete opp1;

		// Perform test
		Test.startTest();
		undelete opp1;
		Test.stopTest();

		// Verify
		Fund__c fundAfterUpdate = [Select Cleared_Investment_Amount__c, Pipeline_Investment_Amount__c from Fund__c where Id= :testFund.Id];
		System.assertEquals(500.00, fundAfterUpdate.Cleared_Investment_Amount__c);
		System.assertEquals(10100.00, fundAfterUpdate.Pipeline_Investment_Amount__c);

	}
}
public with sharing class OpportunityTriggerHandler {

	public static void updateFundsAmount(List<Opportunity> opps) {
		calculateTotalAmount(getFundsWithOpportunities(opps));
	}

	private static List<Fund__c> getFundsWithOpportunities(List<Opportunity> opportunities) {
		Set<Id> fundsIds = new Set<Id>();
		
		for (Opportunity opportunity: opportunities) {
			fundsIds.add(opportunity.Fund__c);
		}

		return [
			SELECT Id, Pipeline_Investment_Amount__c, Cleared_Investment_Amount__c,
				(SELECT Id, Amount, StageName FROM Opportunities__r)
			FROM Fund__c
			WHERE Id IN :fundsIds
		];
	}

	private static void calculateTotalAmount(List<Fund__c> funds) {
		List<Fund__c> summedFunds = new List<Fund__c>();

		for (Fund__c fund: funds) {
			Decimal clearedAmount = 0.0;
			Decimal pipelineAmount = 0.0;

			for (Opportunity opportunity: fund.Opportunities__r) {
				if (opportunity.Amount != null && opportunity.StageName == 'Closed Won') {
					clearedAmount += opportunity.Amount;
				} else if (opportunity.Amount != null && opportunity.StageName != 'Closed Lost') {
					pipelineAmount += opportunity.Amount;
				}
			}

			fund.Cleared_Investment_Amount__c = clearedAmount;
			fund.Pipeline_Investment_Amount__c = pipelineAmount;
			summedFunds.add(fund);
		}

		update summedFunds;
	}

}
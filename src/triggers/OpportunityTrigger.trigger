trigger OpportunityTrigger on Opportunity (after insert, after update, after delete, after undelete) {
    List<Opportunity> fromTrigger = new List<Opportunity>();

    if (Trigger.isAfter && Trigger.isDelete) {
         fromTrigger = Trigger.old;
    } else if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete)) {
        fromTrigger = Trigger.new;
    }

    OpportunityTriggerHandler.updateFundsAmount(fromTrigger);

}